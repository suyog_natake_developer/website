<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>EDIT PRODUCT</title>
 <?php
 include('header_files.php');
 ?>
	</head>
   <?php
   include('include/preloader.php');
		@$status = $_GET['status'];
		require_once("dbhost.php");
		$p_id=$_GET['p_id'];

                                        $query=mysqli_query($con,"SELECT * FROM `product` WHERE `p_id`='$p_id'");
                                        while($row = mysqli_fetch_assoc($query))
                                        {
									            $p_name=$row['p_name'];
	                                            $p_amount=$row['p_amount'];
											
											}
    ?>
   <section class="content">
        <div class="container-fluid">
			<?php
//include('include/preloader.php');
	if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> User Add Sucessfully.
                            </div>';
						}elseif(@$status == 'access')
						{
							echo'<div class="alert alert-danger">
                                <strong>UnAuthorised Access</strong>Access Denied. Login Again
                            </div>';
						}elseif(@$status == 'logout')
						{
							session_start();
	                        unset($_SESSION['SESS_USERNAME']);
							echo'<div class="alert alert-success">
                                <strong>Logged Out</strong> You successfully Login Out.
                            </div>';
						}elseif(@$status == 'invalid')
						{
							session_start();
	                        unset($_SESSION['SESS_USERNAME']);
							echo'<div class="alert alert-success">
                                <strong>Wrong Username & Password</strong> Check Again.
                            </div>';
						}
?>
      
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="header">
                            <h2>
                                EDIT product
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" action="edit-product-exec.php">                                
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Product Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                           <input type="text" id="p_name" value="<?php echo $p_name;?>" name="p_name" class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<input type="hidden" name="p_id" value="<?php echo $p_id;?>">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Product Amount</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="p_amount" value="<?php echo $p_amount;?>" name="p_amount" class="form-control mobile-phone-number" placeholder="Please Type Mobile">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <button type="SUBMIT" class="btn btn-primary waves-effect">
                                                <i class="material-icons">verified_user</i>
                                                <span>SAVE</span>
                                        </button>
                                        <!-- <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button> -->
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <button type="RESET" class="btn bg-brown waves-effect">
                                                <i class="material-icons">report_problem</i>
                                                <span>CLEAR</span>
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
<?php
include('include/footer_files.php');
?>

</body>
</html>