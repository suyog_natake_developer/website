<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM | Add User</title>
    <!-- Favicon-->
 <?php
 include('header_files.php');
 ?>
 </head>
      <?php
	  
	  include('include/preloader.php');
	  ?>
   <section class="content">
        <div class="container-fluid">
   
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
					<?php
						@$status=$_GET['status'];
						if(@$status =='success')
						{
							echo'<div class="alert alert-success">
                                <strong>Success!</strong>Product Added.
                            </div>';
						}
						elseif(@$status == 'added')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Record Aded.
                            </div>';
						}
						elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> You successfully Edited Record.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						elseif(@$status == 'exist')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						elseif(@$status == 'active')
						{
							echo'<div class="alert alert-success">
                                <strong>ACtive</strong>successfully Route.
                            </div>';
						}
						elseif(@$status == 'deactive')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deactive</strong>succesffuly Route.
                            </div>';
						}
						?>             
                                                <div class="header">
                            <h2>
                                ADD PRODUCT
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" action="add-product-exec.php">                                
                            <input type="hidden" id="delete_key" value="12345">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">PRODUCT NAME</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="p_name" class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">PRODUCT AMOUNT</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="mobile" name="p_amount" class="form-control mobile-phone-number" placeholder="Please Type amount" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <button type="SUBMIT" class="btn btn-primary waves-effect">
                                                <i class="material-icons">verified_user</i>
                                                <span>SAVE</span>
                                        </button>
                                        <!-- <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button> -->
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <button type="RESET" class="btn bg-brown waves-effect">
                                                <i class="material-icons">report_problem</i>
                                                <span>CANCEL</span>
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
     <section class="content" style="margin-top:0px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ALL USER	
                            </h2>
                        </div>
						
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>SR.NO</th>
                                            <th>PRODUCT NAME</th>
											<th>PRODUCT AMOUNT</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once("dbhost.php"); 

                                        $queryk = mysqli_query($con,"SELECT * FROM product");
                                        while ($rowk = mysqli_fetch_assoc($queryk))
                                        {
                                       echo'<tr>';
                                       echo'<td>'.$rowk['p_id'].'</td>';
                                       echo'<td>'.$rowk['p_name'].'</td>';
									   echo'<td>'.$rowk['p_amount'].'</td>';
                                       echo'<td><a href="javascript:edit_id('.$rowk['p_id'].')" type="button">
											<i class="material-icons">edit</i>
											</button></a>
											
											   <a href="javascript:delete_id('.$rowk['p_id'].')" type="button">
											<i class="material-icons">delete</i>
											</button></a>
											</td>';
                                       echo'</tr>';
                                        }
                                        ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
	<script>
	
		 function delete_id(str)
 {
	var delete_key =document.getElementById('delete_key').value;
	var kn = prompt("Enter Password");
	if(delete_key == kn)
	{
	   var a =confirm("are you sure?");
	   if(a)
	   {
         window.location.href='delete-product-exec.php?p_id='+str;
	   }
	}
	   else{
		   alert('incorrect password');
	   }
}
function edit_id(str)
{
	var delete_key =document.getElementById('delete_key').value;
	var kn = prompt("Enter Password");
	if(delete_key == kn)
	{
	   var a =confirm("are you sure?");
	   if(a)
	   {
         window.location.href='edit.php?p_id='+str;
		 
		 return 0;
	   }
	}
	   else{
		   alert('incorrect password');
	   }
	}
	</script>
<?php
include('include/footer_files.php');
?>
</body>
</html>