<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM | DAILY ENTRY</title>
	<?php
	include('header_files.php');
	include("dbhost.php");
	?>
 </head>
<?php

include('include/preloader.php');
?>
     <section class="content">
        <div class="container-fluid">
        
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="header">
                            <h2>
                                 SINGLE ENTRY
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" action="add-single-exec.php" onsubmit="event.preventDefault();">                                
                            <input type="hidden" id="delete_key" value="delete">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Select Date</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" id="name" name="date" class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Select Route</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                             <select class="form-control show-tick"  name="r_id" id="r_id" onchange="getCust(this.value)" required="required">
                                                <option value="">-- Please select --</option>
												<?php 
										include('dbhost.php');
										$query=mysqli_query($con,"SELECT * FROM `root`");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['r_id'].'">'.$row['rtname'].'</option>';
                                          
										}
										?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
									
									<div id="txt1"></div>
					
		<button type="SUBMIT" onclick="getStudentList(this.value)" value="kk" class="btn btn-primary m-t-15 waves-effect">SEARCH</button>
                                </div>
						</form>
						</div>
				    </div>
			</div>
		</div>
	</div>
	</section>
    <script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	 <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->
    <script src="js/demo.js"></script>
	  <script language="JavaScript">
	  
  function demo(str)
{
	var a = confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='delete-single.php?s_id='+str; 
		return 0;
	}
}
  function getCust(str) { 
		var r_id=document.getElementById('r_id').value;
		if (str.length == 0) { 
            document.getElementById("txt1").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txt1").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "getCust.php?r_id="+str, true);
            xmlhttp.send();
        }
    }	
  </script>

</body>
</html>