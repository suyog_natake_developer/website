<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM | REPORTS</title>
	<?php
	include('header_files.php');
	include('dbhost.php');
	?>
 </head>
        <body class="theme-red">
    <div class="overlay"></div>
     <section class="content">
        <div class="container-fluid">
            <div class="block-header"> 
                <div class="body">
                    <ol class="breadcrumb breadcrumb-col-teal">
                        <li><a href="dashboard.php"><i class="material-icons">home</i> Home</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">library_books</i> Registration</a></li>
                        <li><a href="add-user.php"><i class="material-icons">archive</i> User</a></li>
                    </ol>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <?php
						if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong>Product Type addedd.
                            </div>';
						}elseif(@$status == 'error')
						{
							echo'<div class="alert alert-danger">
                                <strong>Something!</strong> Went Wrong.
                            </div>';
						}elseif(@$status == 'deletesuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>Deleted</strong> You successfully deleted Record.
                            </div>';
						}
						elseif(@$status == 'editsuccess')
						{
							echo'<div class="alert alert-danger">
                                <strong>EDIT</strong> You successfully Edit Record.
                            </div>';
						}
						?>
                                                <div class="header">
                            <h2 class="card-inside-title">ALL CUSTOMER REPORTS</h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" onsubmit="event.preventDefault()"> 
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="body">
											 <div class="row clearfix">
                                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                        <label for="email_address_2">From Date</label>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                        <div class="form-group">
                                                            <div class="form-line">
                     <input type="date" onchange="getdate1(this.value)" name="fromdate" id="fromdate" class="form-control" placeholder="Please enter a name...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											<div id="txt2"></div>
			                                 <center><button type="SUBMIT" onClick="getLedger(this.value)"; value="kk" class="btn btn-primary m-t-15 waves-effect">SEARCH</button></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
						
							
                        </div>
                    </div>
                </div>
            </div>
     </div>
	   </section>		
<div id="txt3"></div>
	<!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->
    <script src="js/demo.js"></script>
<script>
	function getLedger(str) {
		
	//	alert ('hii');
	//var fromdate = document.getElementById('f_date').value;
	var fromdate = document.getElementById('fromdate').value;
		var todate = document.getElementById('todate').value;
		
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getLedger.php?value="+str+"&fromdate="+fromdate+"&todate="+todate,true);
        xmlhttp.send();
	
		
    }
    }	
  function getCust(str) { 
		
		if (str.length == 0) { 
            document.getElementById("txt1").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txt1").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "getCust.php?r_id="+str, true);
            xmlhttp.send();
        }
    }								
			function getdate1(str) {
		
	//	alert ('hii');
		//var f_date = document.getElementById('f_date').value;
//	var fromdate = document.getElementById('fromdate').value;
	//	var todate = document.getElementById('todate').value;
		
	if (str == "") {
        document.getElementById("txt2").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt2").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getdate1.php?value="+str,true);
        xmlhttp.send();
	
		
    }
    }
	  /*function getCalamt(str) { 
		
		if (str.length == 0) { 
            document.getElementById("txt3").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txt3").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "getCalamt.php?value="+str+"&c_id="+c_id+"&r_id="+r_id,true);
            xmlhttp.send();
			//document.getElementById('date').disabled = false;
        }
    }*/
	function printDiv(divName)
{
var content = document.getElementById(divName).innerHTML;
var orcontent = document.body.innerHTML;
document.body.innerHTML = content;
window.print();
document.body.innerHTML = orcontent;
}	
</script>
</body>
</html>