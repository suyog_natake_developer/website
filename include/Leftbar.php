 <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="Dashboard.php">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">person_add</i>
                            <span>Registration</span>
                        </a>
                        <ul class="ml-menu">
                               <li>
                                <a href="add-user.php">
                                    <i class="material-icons">person</i>
                                    <span>User</span>
                                </a>
                            </li>
                                    <li>
                                        <a href="add-root.php">
                                    <i class="material-icons">location_on</i>
                                    <span>Route</span>
                                </a>
                                    </li>
                                    <li>
                                        <a href="add-product.php">
                                    <i class="material-icons">add_shopping_cart</i>
                                    <span>Product</span>
                                </a>
                                    </li>
									<li>
									     <a href="add-customer.php">
                                    <i class="material-icons">group_add</i>
                                    <span>Customer</span>
                                </a>
							</li>
                        </ul>
                    </li>
                   <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">local_drink</i>
                            <span>JAR Billing</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="single.php">
                                    <i class="material-icons">exposure_plus_1</i>
                                    <span>Single</span>
                                </a>
                            </li>
                            <li>
                                <a href="Jar Setting.php">
                                    <i class="material-icons">exposure_plus_2</i>
                                    <span>Jar Setting</span>
                                </a>
                            </li>
                            
                            
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Payment</span>
                        </a>
                        <ul class="ml-menu">
						<li>
                                <a href="gen-payment.php">
                                    <i class="material-icons">account_balance</i>
                                    <span>Generate Payment</span>
                                </a>
                            </li>
                            <li>
                                <a href="accept-payment.php">
                                    <i class="material-icons">account_balance_wallet</i>
                                    <span>Accept-Payment</span>
                                </a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">home</i>
                            <span>Booking</span>
                        </a>
                        <ul class="ml-menu">
						<li>
                                <a href="advance.php">
                                    <i class="material-icons">account_balance</i>
                                    <span>Advance Booking</span>
                                </a>
                            </li>
                            <li>
                                <a href="booking-bil.php">
                                    <i class="material-icons">account_balance_wallet</i>
                                    <span>Booking Biling</span>
                                </a>
                            </li>
							 <li>
                                <a href="booking-jar.php">
                                    <i class="material-icons">account_balance_wallet</i>
                                    <span>Booking Jar/IN</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="">
                        <a href="counter.php">
                            <i class="material-icons">invert_colors</i>
                            <span>Counter</span>
                        </a>
                    </li>
                      <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">report</i>
                            <span>Report</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="cust-ledger.php">
                                    <i class="material-icons">account_balance</i>
                                    <span>Customer Ledger(Date Wise)</span>
                                </a>
                            </li>
							
							<li>
                                <a href="cust-d2d.php">
                                    <i class="material-icons">account_balance</i>
                                    <span>Customer Day to Day Report</span>
                                </a>
                            </li>
									
							<li>
                                <a href="payment-report.php">
                                    <i class="material-icons">account_balance</i>
                                    <span>Generate Payment REPORTS</span>
                                </a>
                            </li>
							
							<li>
                                <a href="counter-sale.php">
                                    <i class="material-icons">account_balance_wallet</i>
                                    <span>Counter Sales Report</span>
                                </a>
                            </li>
                            
                        </ul>
                    </li>
					</ul>
					</div>
            <div class="legal">
                <div class="copyright">
                    &copy; 2019 - 2020 <a href="javascript:void(0);">TechnoSoft-Shrirampur</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
        </aside>