<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
@$status=@$_GET['status'];
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM | Add ROOT</title>
    <?php
	include('header_files.php');
	?>
	<style>
	img{
		height:30px;
	}
	
	</style>
  </head>
        <body class="theme-red"> <section class="content">
        <div class="container-fluid">
<?php
include('include/preloader.php');

?>
            <div class="block-header"> 
                <div class="body">
                    <ol class="breadcrumb breadcrumb-col-teal">
                        <li><a href="dashboard.php"><i class="material-icons">home</i> Home</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">library_books</i> Registration</a></li>
                        <li><a href="add-user.php"><i class="material-icons">archive</i> User</a></li>
                    </ol>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
								<?php
				if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> User Add Sucessfully.
                            </div>';
						}elseif(@$status == 'edit')
						{
							echo'<div class="alert alert-sucess">
                                <strong>Edit</strong>User Edit
                            </div>';
						}elseif(@$status == 'Invalid')
						{
							session_start();
	                        unset($_SESSION['SESS_USERNAME']);
							echo'<div class="alert alert-danger">
                                <strong>User</strong>Not Added.
                            </div>';
						}elseif(@$status == 'delete')
						{
							session_start();
	                        unset($_SESSION['SESS_USERNAME']);
							echo'<div class="alert alert-danger">
                                <strong>Sucessful</strong> delete.
                            </div>';
						}
?>
                           <div class="header">
                            <h2>
                                USERS REGISTRATION
                            </h2>
                        </div>
              <div class="body">
                            <form class="form-horizontal" method="POST" action="add-root-exec.php">                                
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">ROUTE Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="rtname" class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <button type="SUBMIT" class="btn btn-primary waves-effect">
                                                <i class="material-icons">verified_user</i>
                                                <span>SAVE</span>
                                        </button>
                                        <!-- <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button> -->
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <button type="RESET" class="btn bg-brown waves-effect">
                                                <i class="material-icons">report_problem</i>
                                                <span>CANCEL</span>
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content" style="margin-top:0px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
				
   
                        <div class="header">
                            <h2>
                                ALL USER	
                            </h2>
                        </div>
						
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>SR.NO</th>
                                            <th>ROUTE NAME</th>
											<th>Status</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        require_once("dbhost.php"); 

                                        $queryk = mysqli_query($con,"SELECT * FROM `root`");
                                        while ($rowk = mysqli_fetch_assoc($queryk))
                                        {
                                       echo'<tr>';
                                       echo'<td>'.$rowk['r_id'].'</td>';
                                       echo'<td>'.$rowk['rtname'].'</td>';
									   $active_status=$rowk['active_status'];
									   if($active_status=='active')
									   {
										     echo'<td><a href="javascript:deactive('.$rowk['r_id'].')" type="button">
											<img src="green.jpg"></i>
											</button></a>';
									   }
									   else
									   {
										    echo'<td><a href="javascript:active('.$rowk['r_id'].')" type="button" >
											<img src="red.png"></i>
											</button></a>';
									   }
                                       echo'<td>
									   <a href="javascript:edit_id('.$rowk['r_id'].')" type="button">
											<i class="material-icons">edit</i>
											</button></a>
											
											<a href="javascript:delete_id('.$rowk['r_id'].')" type="button">
											<i class="material-icons">delete</i>
											</button></a>
											</td>';
                                       echo'</tr>';
                                        }
                                        ?>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
		 <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->
    <script src="js/demo.js"></script>
<script>
		 function delete_id(r_id)
 {

	   var a =confirm("are you sure?");
	   if(a)
	   {
         window.location.href='delete-root-exec.php?r_id='+r_id;
		 return 0;
	   }
	
}
	function edit_id(r_id)
{
	var a=confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='edit-root.php?r_id='+r_id; 
		return 0;
	}
}

	function deactive(str)
{
	var a=confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='deactive-root.php?r_id='+str; 
		return 0;
	}
}
function active(str)
{
	var a=confirm("Are You Sure...?");
	if(a)
	{
		window.location.href='active-root.php?r_id='+str; 
		return 0;
	}
}
</script>


</body>
</html>
