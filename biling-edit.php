<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM | EDIT BILING</title>
	  <?php
		@$status = $_GET['status'];
		require_once("dbhost.php");
		$b_id=$_GET['b_id'];

                                        $query=mysqli_query($con,"SELECT * FROM `biling` WHERE `b_id`='$b_id'");
                                        while($row = mysqli_fetch_assoc($query))
                                        {
									  // $a_id=$_POST['a_id'];
	                                         $date=$row['date'];
											 $bcust=$row['bcust'];
								             $rate=$row['rate'];
											 $quantity=$row['quantity'];
											 $amount=$row['amount'];
											
											}
    ?>
	<?php
	include('header_files.php');
	?>
 </head>
        <body class="theme-red">
    <div class="overlay"></div>
     <section class="content">
        <div class="container-fluid">
            <div class="block-header"> 
                <div class="body">
                    <ol class="breadcrumb breadcrumb-col-teal">
                        <li><a href="dashboard.php"><i class="material-icons">home</i> Home</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">library_books</i> Registration</a></li>
                        <li><a href="add-user.php"><i class="material-icons">archive</i> User</a></li>
                    </ol>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="header">
                            <h2>
                               Edit Booking 
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" action="edit-biling-exec.php">                                
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Select Date</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="date" value="<?php echo $date;?>"class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<input type="hidden" name="b_id" value="<?php echo $b_id ?>" />
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Customer Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text"  name="bcust" value="<?php echo $bcust;?>" class="form-control mobile-phone-number" placeholder="Please Type Mobile">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Rate</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text"  name="rate" value="<?php echo $rate;?>" class="form-control mobile-phone-number" placeholder="Please Type Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
								 <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Quantity</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="quantity" value="<?php echo $quantity;?>" class="form-control mobile-phone-number" placeholder="Please Type Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
							 <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Amount</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="amount" value="<?php echo $amount;?>" class="form-control mobile-phone-number" placeholder="Please Type Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <button type="SUBMIT" class="btn btn-primary waves-effect">
                                                <i class="material-icons">update</i>
                                                <span>Save</span>
                                        </button>
                                        <!-- <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button> -->
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <button type="RESET" class="btn bg-brown waves-effect">
                                                <i class="material-icons">report_problem</i>
                                                <span>CLEAR</span>
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->
    <script src="js/demo.js"></script>
    <script>