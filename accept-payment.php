<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM |ACCEPT Payment</title>
	<?php
	include('header_files.php');
	include('dbhost.php');
	?>
 </head>

<?php

include('include/preloader.php');
?>
     <section class="content">
        <div class="container-fluid">
            <div class="block-header"> 
                <div class="body">
                    <ol class="breadcrumb breadcrumb-col-teal">
                        <li><a href="dashboard.php"><i class="material-icons">home</i> Home</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">library_books</i> Registration</a></li>
                        <li><a href="add-user.php"><i class="material-icons">archive</i> User</a></li>
                    </ol>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="header">
                            <h2>
                                ACCEPT PAYMENT
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST"  action="payment-exec.php">                                
                            <input type="hidden" id="delete_key" value="delete">
								     <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">From Date</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" id="f_date" name="f_date" class="datepicker form-control" placeholder="Please Type Date" />
                                            </div>
                                        </div>
                                    </div>	
                                </div>
	     <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">TO Date</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" id="t_date" name="t_date" class="datepicker form-control" placeholder="Please Type Date" />
                                            </div>
                                        </div>
                                    </div>	
                                </div>
                                    <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Select Route</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                      <select class="form-control show-tick" name="r_id" id="r_id"  onchange="getCust1(this.value)"  required="required">
                                                <option value="">-- Please select --</option>
					<?php 
										include('dbhost.php');
										$query=mysqli_query($con,"SELECT * FROM `root` WHERE `active_status`='active'");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['r_id'].'">'.$row['rtname'].'</option>';
                                          
										}
										?>                                      </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div id="txt1"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<script>
	function getPayment(str) {
				
				var r_id=document.getElementById('r_id').value;
				var c_id=document.getElementById('c_id').value;
				var f_date=document.getElementById('f_date').value;
				var t_date=document.getElementById('t_date').value;

		
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getPayment.php?r_id="+r_id+"&c_id="+c_id+"&f_date="+f_date+"&t_date="+t_date,true);
        xmlhttp.send();
	
		
    }
    }
						
			function getCust1(str) {
				
				var r_id=document.getElementById('r_id').value;
				var f_date=document.getElementById('f_date').value;
				var t_date=document.getElementById('t_date').value;

		
	if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getCust1.php?r_id="+r_id+"&f_date="+f_date+"&t_date="+t_date,true);
        xmlhttp.send();
	
		
    }
    }
						
</script>
	  <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->
    <script src="js/demo.js"></script>