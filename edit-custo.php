<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM | Add User</title>
	  <?php
		@$status = $_GET['status'];
		require_once("dbhost.php");
		$c_id=$_GET['c_id'];

                                        $query=mysqli_query($con,"SELECT * FROM `customer1` WHERE `c_id`='$c_id'");
                                        while($row = mysqli_fetch_assoc($query))
                                        {
											//$c_id=$row['c_id'];
										    $name=$row['name'];
											$mobile=$row['mobile'];
											$p_name=$row['p_name'];
											$r_id=$row['r_id'];
												$address=$row['address'];
											$date=$row['date'];
											
											}
    ?>
	<?php
	include('header_files.php');
	?>
 </head>
        <body class="theme-red">
    <div class="overlay"></div>
     <section class="content">
        <div class="container-fluid">
            <div class="block-header"> 
                <div class="body">
                    <ol class="breadcrumb breadcrumb-col-teal">
                        <li><a href="dashboard.php"><i class="material-icons">home</i> Home</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">library_books</i> Registration</a></li>
                        <li><a href="add-user.php"><i class="material-icons">archive</i> User</a></li>
                    </ol>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="header">
                            <h2>
                                  CUSTOMER
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" action="edit-custo-exec.php">                                
                            <input type="hidden" id="delete_key" value="delete">
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Customers code</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="c_id" value="<?php echo $c_id;?>"class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Customer Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="name" value="<?php echo $name;?>" class="form-control mobile-phone-number" placeholder="Please Type Mobile">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Mobile Number</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="mobile" name="mobile" value="<?php echo $mobile;?>" class="form-control mobile-phone-number" placeholder="Please Type Email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Select Product</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="prdname" name="p_name"  value="<?php echo $p_name;?>" class="form-control show-tick" required="required">
                                                <option value="">-- Please select --</option>
																<?php 
										include('dbhost.php');
										$query=mysqli_query($con,"SELECT * FROM `product`");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['p_name'].'">'.$row['p_name'].'</option>';
                                          
										}
										?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Select Route</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="address" name="address" value="<?php echo $address;?>" class="form-control show-tick" required="required">
                                                <option value="">-- Please select --</option>
												<?php 
										include('dbhost.php');
										$query=mysqli_query($con,"SELECT * FROM `root`");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['rtname'].'">'.$row['rtname'].'</option>';
                                          
										}
										?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <button type="SUBMIT" class="btn btn-primary waves-effect">
                                                <i class="material-icons">update</i>
                                                <span>REGISTER</span>
                                        </button>
                                        <!-- <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button> -->
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <button type="RESET" class="btn bg-brown waves-effect">
                                                <i class="material-icons">report_problem</i>
                                                <span>CLEAR</span>
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
<?php
include('include/footer_files.php');
?>