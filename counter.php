<!DOCTYPE html>
<html>
<?php
include('include/navbar.php');
include('include/Leftbar.php');
include('include/Rightbar.php');
include('include/searchbar.php');
?>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>WATER SYSTEM |COUNTER</title>
    <!-- Favicon-->
 <?php
 include('header_files.php');
 ?>
 </head>
        <body class="theme-red">
    <div class="overlay"></div>
   <section class="content">
        <div class="container-fluid">
            <div class="block-header"> 
                <div class="body">
                    <ol class="breadcrumb breadcrumb-col-teal">
                        <li><a href="dashboard.php"><i class="material-icons">home</i> Home</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">library_books</i> Registration</a></li>
                        <li><a href="add-user.php"><i class="material-icons">archive</i> User</a></li>
                    </ol>
                </div>
            </div>
						<?php
include('include/preloader.php');
	if(@$status == 'success')
						{
							echo'<div class="alert alert-success">
                                <strong>Well done!</strong> Login Success.
                            </div>';
						}elseif(@$status == 'access')
						{
							echo'<div class="alert alert-danger">
                                <strong>UnAuthorised Access</strong>Access Denied. Login Again
                            </div>';
						}elseif(@$status == 'Invalid')
						{
							session_start();
	                        unset($_SESSION['SESS_USERNAME']);
							echo'<div class="alert alert-success">
                                <strong>Logged Out</strong> You successfully Login Out.
                            </div>';
						}elseif(@$status == 'invalid')
						{
							session_start();
	                        unset($_SESSION['SESS_USERNAME']);
							echo'<div class="alert alert-success">
                                <strong>Wrong Username & Password</strong> Check Again.
                            </div>';
						}
?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="header">
                            <h2>
                                COUNTER
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-horizontal" method="POST" action="counter-exec.php">                                
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Select Date</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" name="date" class="form-control" placeholder="Please Type Your Name" autofocus="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="password_2">Product</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                            <select name="product" name="product" id="p_id" onchange="getRate(this.value)" class="form-control show-tick" required="required">
                                                <option value="">-- Please select --</option>
					<?php 
										include('dbhost.php');
										$query=mysqli_query($con,"SELECT * FROM `product`");
                                        while ($row = mysqli_fetch_assoc($query))
                                        {		
												echo'<option value="'.$row['p_id'].'">'.$row['p_name'].'</option>';
                                          
										}
										?>                                      </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div id="txt3"></div>
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <button type="SUBMIT" class="btn btn-primary waves-effect">
                                                <i class="material-icons">verified_user</i>
                                                <span>SAVE</span>
                                        </button>
                                        <!-- <button class="btn btn-primary waves-effect" data-type="success">CLICK ME</button> -->
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <button type="RESET" class="btn bg-brown waves-effect">
                                                <i class="material-icons">report_problem</i>
                                                <span>CANCEL</span>
                                            </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
     <section class="content" style="margin-top:0px;">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                All COUNTER SELLS
                            </h2>
                        </div>
						
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>SR.NO</th>
                                            <th>Date</th>
											<th>Product</th>
                                            <th>Rate</th>
											<th>Quantity</th>
											<th>Amount</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                               <?php
                                        require_once("dbhost.php"); 

                                        $queryk = mysqli_query($con,"SELECT * FROM `counter`");
                                        while ($rowk = mysqli_fetch_assoc($queryk))
                                        {
                                       echo'<tr>';
                                       echo'<td>'.$rowk['cnt_id'].'</td>';
									   echo'<td>'.$rowk['date'].'</td>';
									   echo'<td>'.$rowk['product'].'</td>';
									   echo'<td>'.$rowk['rate'].'</td>';
									   echo'<td>'.$rowk['quantity'].'</td>';
									   echo'<td>'.$rowk['amount'].'</td>';
                                       echo'<td><a href="edit-counter.php?cnt_id='.$rowk['cnt_id'].'" data-toggle="tooltip">
											<i class="material-icons">edit</i>
											</button></a>
											<a href="delete-counter.php?cnt_id='.$rowk['cnt_id'].'"  data-toggle="tooltip">
											<i class="material-icons">delete</i>
											</button></a>
											</td>';
                                       echo'</tr>';
									
                                        }
                                        ?>                                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>
 <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <!-- Demo Js -->
    <script src="js/demo.js"></script>
	<script>
	
function getRate(str) {
	var p_id = document.getElementById('p_id').value;
	if (str == "") {
        document.getElementById("txt3").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt3").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getRate.php?value="+str+"&p_id="+p_id,true);
        xmlhttp.send();
	
		
    }
    }
</script>
</body>
</html>
